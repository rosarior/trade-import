#!/usr/bin/env python3

"""
Copyright 2020 Roberto Rosario

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

__author__ = 'Roberto Rosario'
__author_email__ = 'roberto.rosario.gonzalez@gmail.com'
__copyright_short__ = '2020 Roberto Rosario'
__copyright__ = '{} {}'.format('Copyright', __copyright_short__)
__description__ = 'Trading activity imported and formatter.'
__license__ = 'Apache 2.0'
__title__ = 'Trade Import'
__version__ = '1.1.0'
__website__ = 'https://gitlab.com/rosarior/trade-import'

import argparse
import csv
import datetime
import decimal
import itertools


def get_attribute(obj, attribute_name):
    """
    Recursive attribute resolver that reduces an attribute path to
    allow sorting or grouping by that attribute. Attribute paths
    are dotted as in the Python REPL.
    """
    if '.' in attribute_name:
        attribute_name, parts = attribute_name.split('.', 1)
        obj = getattr(obj, attribute_name)
        return get_attribute(obj=obj, attribute_name=parts)
    else:
        try:
            # Try as a dictionary
            return obj[attribute_name]
        except TypeError:
            try:
                # Try as a function
                return getattr(obj, attribute_name)()
            except TypeError:
                return getattr(obj, attribute_name)


class Filter:
    _registry = {}

    @staticmethod
    def type_cast(string):
        if string[0] in ['"', "'"]:
            return string[1:-1]
        elif '.' in string:
            return decimal.Decimal(string)
        else:
            return int(string)

    @classmethod
    def register(cls, klass):
        cls._registry[klass.identifier] = klass

    @classmethod
    def get(cls, identifier):
        return cls._registry[identifier]

    @classmethod
    def parse(cls, filter_string):
        """
        Split the filter string into its different parts:
        - field
        - comparison (operator)
        - argument (operand)
        """
        try:
            field_identifier, argument = filter_string.split('=')
            field, identifier = field_identifier.split('__')
        except Exception as exception:
            raise TypeError(
                'Invalid filter string: {}'.format(filter_string)
            ) from exception

        try:
            argument = Filter.type_cast(string=argument)
        except Exception as exception:
            raise TypeError(
                'Unable to type cast argument: {}'.format(argument)
            ) from exception

        try:
            return cls.get(identifier=identifier)(
                field=field, argument=argument
            )
        except KeyError:
            raise KeyError('Unknown filter identifier: {}'.format(identifier))

    @classmethod
    def parse_list(cls, filter_string_list):
        """
        Parse a list of filter strings and return a list of filter
        instances.
        """
        result = []

        for filter_string in filter_string_list:
            result.append(
                cls.parse(filter_string=filter_string)
            )

        return result

    def __init__(self, field, argument):
        self.field = field
        self.argument = argument

    def __repr__(self):
        """
        Visual representation of the filter and its parts.
        """
        return '{self.field} {self.label} {self.argument} ({argument_type})'.format(
            self=self, argument_type=type(self.argument)
        )

    def evaluate(self, obj):
        """
        Evaluate the filter against an object. First reduce the
        attribute name in case it is nested. The get_attribute()
        function takes case of the object type. Then call the subclass
        private _evaluate method to compare the extracted value from
        the attribute. This way, repeated code in subclasses is
        reduced.
        """
        if obj:
            value = get_attribute(obj=obj, attribute_name=self.field)
            if self._evaluate(value=value):
                return obj


class FilterEqual(Filter):
    identifier = 'eq'
    label = 'equals'

    def _evaluate(self, value):
        return value == self.argument


class FilterEndsWith(Filter):
    identifier = 'endswith'
    label = 'ends with'

    def _evaluate(self, value):
        return value.endswith(self.argument)


class FilterGreaterThan(Filter):
    identifier = 'gt'
    label = 'greater than'

    def _evaluate(self, value):
        return value > self.argument


class FilterGreaterThanEqual(Filter):
    identifier = 'gte'
    label = 'greater than or equal'

    def _evaluate(self, value):
        return value >= self.argument


class FilterLessThan(Filter):
    identifier = 'lt'
    label = 'less than'

    def _evaluate(self, value):
        return value < self.argument


class FilterLessThanEqual(Filter):
    identifier = 'lte'
    label = 'less than or equal'

    def _evaluate(self, value):
        return value <= self.argument


class FilterNotEqual(Filter):
    identifier = 'ne'
    label = 'no equal'

    def _evaluate(self, value):
        return value != self.argument


class FilterStartsWith(Filter):
    identifier = 'startswith'
    label = 'starts with'

    def _evaluate(self, value):
        return value.startswith(self.argument)


Filter.register(klass=FilterEndsWith)
Filter.register(klass=FilterEqual)
Filter.register(klass=FilterGreaterThan)
Filter.register(klass=FilterGreaterThanEqual)
Filter.register(klass=FilterLessThan)
Filter.register(klass=FilterLessThanEqual)
Filter.register(klass=FilterNotEqual)
Filter.register(klass=FilterStartsWith)


class ApexImporter:
    """
    Class that takes a CSV rows, interprets it and returns a common
    trade class instance.
    """
    @classmethod
    def import_row(cls, row):
        """
        Takes a dictionary corresponding to a CSV row and returns a
        Trade instance
        """
        trade = Trade()
        # Symbol
        # SPXW  20200511P 2835.000
        # 012345678901234567890123
        # Ticker, expiration, option type, strike
        expiration_year = int(row['Symbol'][6:10])
        expiration_month = int(row['Symbol'][10:12])
        expiration_day = int(row['Symbol'][12:14])

        trade.expiration = datetime.date(
            year=expiration_year, month=expiration_month,
            day=expiration_day
        )

        trade.net_amount = decimal.Decimal(row['Net Amount'][1:])

        if row['Trade Action'].endswith('OPEN'):
            trade.position = Trade.POSITION_OPEN
        elif row['Trade Action'].endswith('CLOSE'):
            trade.position = Trade.POSITION_CLOSE
        else:
            trade.position = None

        trade.price = float(row['Price'][1:])

        trade.quantity = int(row['Qty'])

        trade.strike = decimal.Decimal(row['Symbol'][16:24])

        trade.symbol = row['Symbol'][0:4]

        # Timestamp
        # 2020-05-06T10:16:26.000
        # 01234567890123456789012
        # Date, time
        trade_date_year = int(row['Timestamp'][0:4])
        trade_date_month = int(row['Timestamp'][5:7])
        trade_date_day = int(row['Timestamp'][8:10])
        trade_date_hour = int(row['Timestamp'][11:13])
        trade_date_minute = int(row['Timestamp'][14:16])
        trade_date_second = int(row['Timestamp'][17:19])

        trade.trade_datetime = datetime.datetime(
            year=trade_date_year, month=trade_date_month,
            day=trade_date_day, hour=trade_date_hour,
            minute=trade_date_minute, second=trade_date_second
        )

        trade_type = row['Symbol'][14:15]

        if trade_type == 'C':
            trade.trade_type = Trade.TYPE_CALL
        elif trade_type == 'P':
            trade.trade_type = Trade.TYPE_PUT
        else:
            trade.trade_type = None

        if row['Trade Action'].startswith('BUY'):
            trade.action = Trade.ACTION_BUY
        elif row['Trade Action'].startswith('SELL'):
            trade.action = Trade.ACTION_SELL
        elif row['Description'].endswith('OPTION EXPIRATION - EXPIRED'):
            trade.action = Trade.ACTION_EXPIRATION
        else:
            trade.action = None

        trade.dte = (trade.expiration - trade.trade_datetime.date()).days

        return trade


class Trade:
    # Internal class constants. Avoid magic numbers and provide display
    # strings.
    ACTION_BUY = 1
    ACTION_SELL = 2
    ACTION_EXPIRATION = 3
    ACTION_DISPLAY = {
        ACTION_BUY: '+',
        ACTION_SELL: '-',
        ACTION_EXPIRATION: '(expired)',
    }
    POSITION_OPEN = 1
    POSITION_CLOSE = 2
    TYPE_CALL = 1
    TYPE_PUT = 2
    TYPE_DISPLAY = {
        TYPE_CALL: 'Call',
        TYPE_PUT: 'Put',
    }

    def __repr__(self):
        """
        The representation of the trade instance when printed alone.
        """
        return '{self.trade_datetime}: {self.symbol} {type_display}{action_display} {self.strike} {self.expiration} $ {self.net_amount}'.format(
            self=self,
            action_display=self.get_action_display(),
            type_display=self.get_type_display()
        )

    def display(self):
        """
        The representation of the trade instance when printed in
        relation to a trade event.
        """
        return '{self.symbol} {type_display:4} {self.strike} ({self.dte} days) [{quantity_display} @ {self.price:.2f}] $ {net_amount_display}'.format(
            self=self,
            net_amount_display=self.get_net_amount_display(),
            quantity_display=self.get_quantity_display(),
            type_display=self.get_type_display()
        )

    def get_action_display(self):
        """
        Returns the text for the trade action value.
        """
        return Trade.ACTION_DISPLAY.get(self.action) or ''

    def get_net_amount_display(self):
        """
        Returns the formated display for the net amount of the trade.
        Allows easy customization to use accounting style negative
        values.
        """
        if self.net_amount > 0:
            return '+{}'.format(self.net_amount)
        else:
            return '{}'.format(self.net_amount)

    def get_quantity_display(self):
        """
        Returns the formated display for the trade quantity. By
        defaults trades are displayed with a plus (+) or minus (-)
        signs when bought or sold.
        """
        if self.quantity > 0:
            return '+{}'.format(self.quantity)
        else:
            return '{}'.format(self.quantity)

    def get_type_display(self):
        """
        Returns the formated display for the trade type (Call option,
        Put option, etc). Allows for easy expantion of other trade
        types like stock.
        """
        return Trade.TYPE_DISPLAY.get(self.trade_type) or ''


class CSVReader:
    @staticmethod
    def group_by(entries, attribute):
        """
        Takes a list of items and groups them by the values of one of
        the common attributes. Returns a dictionary of grouping values
        and a list of the elements for that value.
        """
        def key(row):
            return get_attribute(obj=row, attribute_name=attribute)

        return dict(
            [
                (
                    value, list(items)
                ) for value, items in itertools.groupby(entries, key)
            ]
        )

    @staticmethod
    def sort_by(entries, attribute):
        """
        Returns a sorted list of items. The sorting key can be any
        common attribute of the list members.
        """
        def key(row):
            return getattr(row, attribute)

        return sorted(entries, key=key)

    def __init__(self, filename):
        self.filename = filename
        self.trades = []

    def load(self, csv_filter_string_list):
        """
        Iterate over the CSV rows and call the imported to interpret
        them.
        """
        csv_filters = Filter.parse_list(
            filter_string_list=csv_filter_string_list or []
        )

        with open(self.filename, newline='') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                for entry in csv_filters:
                    row = entry.evaluate(obj=row)

                if row:
                    try:
                        self.trades.append(ApexImporter.import_row(row=row))
                    except Exception as exception:
                        raise exception.__class__(
                            'Unable to import row: {}'.format(row)
                        ) from exception

    def display(self, trade_filter_string_list=None):
        """
        Return a formated list of trades by date, then by time, also
        groups trades that are part of a spread and identifies the
        spread type. Provides a spread, day and grand total for trade
        net amounts.
        """
        trade_filters = Filter.parse_list(
            filter_string_list=trade_filter_string_list or []
        )

        result = []
        for trade in self.trades:
            for filter_entry in trade_filters:
                trade = filter_entry.evaluate(obj=trade)

            if trade:
                result.append(trade)

        trades = result

        # Group trades by date
        trades = CSVReader.group_by(
            entries=trades, attribute='trade_datetime.date'
        )
        grand_total = 0

        for trade_day, day_entries in trades.items():
            print(trade_day)
            print(10 * '-')
            day_balance = 0

            # Narrow day trades by time
            day_entries = CSVReader.group_by(
                entries=day_entries, attribute='trade_datetime.time'
            )
            for trade_time, time_entries in day_entries.items():
                # Trade day
                print(trade_time)

                # Sort trades by strike value, smaller first for
                # easier visual scan.
                time_entries = CSVReader.sort_by(
                    entries=time_entries, attribute='strike'
                )
                for time_entry in time_entries:
                    day_balance = day_balance + time_entry.net_amount

                    print('  ', time_entry.display())

                # Analize the trades and see if they are part of a
                # spread and which kind of spread.
                type_string = None
                if len(time_entries) == 2:
                    if time_entries[0].trade_type == Trade.TYPE_PUT and time_entries[1].trade_type == Trade.TYPE_PUT:
                        # 2 option put spread
                        if not(time_entries[0].strike == time_entries[1].strike):
                            # Discard same strike options

                            if time_entries[0].strike > time_entries[1].strike:
                                trade_smaller = time_entries[1]
                                trade_bigger = time_entries[0]
                            else:
                                trade_smaller = time_entries[0]
                                trade_bigger = time_entries[1]

                            if trade_smaller.action == Trade.ACTION_BUY and trade_bigger.action == Trade.ACTION_SELL:
                                if trade_smaller.position == Trade.POSITION_OPEN:
                                    type_string = 'Open put credit spread'
                                else:
                                    type_string = 'Close put debit spread'

                            elif trade_smaller.action == Trade.ACTION_SELL and trade_bigger.action == Trade.ACTION_BUY:
                                if trade_smaller.position == Trade.POSITION_OPEN:
                                    type_string = 'Open put debit spread'
                                else:
                                    type_string = 'Close put credit spread'
                    elif time_entries[0].trade_type == Trade.TYPE_CALL and time_entries[1].trade_type == Trade.TYPE_CALL:
                        # 2 option call spread
                        if not(time_entries[0].strike == time_entries[1].strike):
                            # Discard same strike options

                            if time_entries[0].strike > time_entries[1].strike:
                                trade_smaller = time_entries[1]
                                trade_bigger = time_entries[0]
                            else:
                                trade_smaller = time_entries[0]
                                trade_bigger = time_entries[1]

                            if trade_smaller.action == Trade.ACTION_BUY and trade_bigger.action == Trade.ACTION_SELL:
                                if trade_smaller.position == Trade.POSITION_OPEN:
                                    type_string = 'Open call debit spread'
                                else:
                                    type_string = 'Close call credit spread'

                            elif trade_smaller.action == Trade.ACTION_SELL and trade_bigger.action == Trade.ACTION_BUY:
                                if trade_smaller.position == Trade.POSITION_OPEN:
                                    type_string = 'Open call credit spread'
                                else:
                                    type_string = 'Close call debit spread'
                elif len(time_entries) == 4:
                    # A 4 option spread.
                    # Sort spread by strike price again just in case
                    # as dictionaries are not ordered in Python < 3.8.
                    spread_trades = sorted(
                        time_entries, key=lambda x: x.strike
                    )

                    # To avoid a single very long if statement,
                    # split the conditional for the iron condor into a
                    # nested conditional.
                    if spread_trades[0].trade_type == Trade.TYPE_PUT and spread_trades[0].action == Trade.ACTION_SELL:
                        if spread_trades[1].trade_type == Trade.TYPE_PUT and spread_trades[1].action == Trade.ACTION_BUY:
                            if spread_trades[2].trade_type == Trade.TYPE_CALL and spread_trades[2].action == Trade.ACTION_BUY:
                                if spread_trades[3].trade_type == Trade.TYPE_CALL and spread_trades[3].action == Trade.ACTION_SELL:
                                    if spread_trades[0].position == Trade.POSITION_CLOSE:
                                        type_string = 'Reverse iron condor'

                if type_string:
                    time_total = 0
                    for time_entry in time_entries:
                        time_total = time_total + time_entry.net_amount

                    print('   # {}: $ {}'.format(type_string, time_total))

            print('Day total: $ {}'.format(day_balance))
            grand_total = grand_total + day_balance

            print()

        print('Grand total: $ {}'.format(grand_total))

        print()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Import an APEX Clearing activity CSV file.'
    )
    parser.add_argument(
        'filename', metavar='filename', type=str,
        help='path to the CSV file to import.'
    )
    parser.add_argument(
        '--csv-filter', metavar='csv_filter', action='extend', type=str,
        nargs='+', help='filters to apply while importing the CSV.'
    )
    parser.add_argument(
        '--trade-filter', metavar='trade_filter', action='extend',
        type=str, nargs='+',
        help='filters to apply while printing the trade report.'
    )

    args = parser.parse_args()
    instance = CSVReader(filename=args.filename)
    instance.load(csv_filter_string_list=args.csv_filter)
    instance.display(trade_filter_string_list=args.trade_filter)
