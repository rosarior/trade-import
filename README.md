# Installation

Just copy the tradeimport.py file and make it executable.

# Usage

Export the "Activity" information of your account from the Apex website.

https://tastyworks.freshdesk.com/support/solutions/articles/43000524838-how-to-create-apex-credentials-for-tradelog-users-

Call ``tradeimport.py`` with the resulting CSV file as the argument.

    ./tradeimport.py activity-export-2020-05.csv

# Limitations

- Only Apex Clearing Activity CSVs are supported.
- Only rows of type "TRADES" are supported.

# Examples

Import the rows of type "TRADES".

    ./tradeimport.py --csv-filter='Type__eq="TRADES"' activity-export-2020-05.csv

Import the rows of type "TRADES" and with a symbol field content that starts with "SPXW".

    ./tradeimport.py --csv-filter='Type__eq="TRADES"' --csv-filter='Symbol__startswith="SPXW"' activity-export-2020-05.csv

Display trades with a net amount greater than 100.

    ./tradeimport.py --trade-filter='net_amount__gt=100' activity-export-2020-05.csv

Display trades with a DTE lower than 30.

    ./tradeimport.py --trade-filter='dte__lt=30' activity-export-2020-05.csv

# Disclaimers

Neither tastyworks nor Apex Clearing are not affiliated with the
makers of this program and do not endorse this product.

This program does not provide investment, tax, or legal advice.

There are no warranties, either expressed or implied about the
functionality or results provided by this program.
