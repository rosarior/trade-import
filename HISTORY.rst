1.0.0 (2020-05-13)
==================
- Support import of Apex Clearing Activity CSV files.

1.1.0 (2020-05-15)
==================
- Support filtering CSV rows during import.
- Support filtering trades during display.
